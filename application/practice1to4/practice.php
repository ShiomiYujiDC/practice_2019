<!DOCTYPE html>
<html>
<head>
  <title>PHP練習問題</title>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="practice_stylesheet.css">
</head>
<body>
  <h1>PHP練習問題</h1>
  <br>
  <!-- ここに好きな言葉をechoしてみましょう -->
  <?php 
    echo "今から課題を始めます";
  ?>
  <!-- 
  ▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼　
  【問題】おみくじ1
  ▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
  -->
  <div class="font01">【問題】おみくじ1</div>
  <div class="box7"><p>
    <!-- おみくじ出力場所 -->
    <?php 
      // $ary_omikuji = array("大吉","中吉","小吉","末吉","吉","凶");
      // shuffle($ary_omikuji);
      // echo "今日の運勢は【".$ary_omikuji[0]."】";
    ?> 

    <?php 
      // $ary_omikuji2 = array("大吉","中吉","小吉","末吉","吉","凶");
      // $index = rand(0, 5);
      // echo "今日の運勢は【".$ary_omikuji2[$index]."】";
    ?> 

    <?php 
      $ary_omikuji3 = array("大吉","中吉","小吉","末吉","吉","凶");
      $rand_keys = array_rand($ary_omikuji3, 1);
      echo "今日の運勢は【".$ary_omikuji3[$rand_keys]."】";
    ?> 

  </p></div>
  <br><br>
  <!-- 
  ▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼　
  【問題】おみくじ2
  ▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
  -->
  <div class="font01">【問題】おみくじ2</div>
  <div class="box7"><p>
    <!-- おみくじ出力場所 -->
    <?php 
      $ary_omikuji = array("大吉","中吉","小吉","末吉","吉","凶");
      shuffle($ary_omikuji);
      echo date("Y年n月j日") . "の運勢は【" . $ary_omikuji[0] . "】";
    ?>
  </p></div>
  <br><br>
  <!-- 
  ▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼　
  【問題】数字を文字に変換
  ▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
  -->
  <div class="font01">【問題】数字を文字に変換</div>
    <!-- ここにHTMLでフォームを追加 -->
      <form action="practice.php" method="post">
        <input type="text" name="number" class="text01" placeholder="カンマ区切りで数字を入力して変換">
        <input type="submit" class="btn-gradient-3d" value="変換">
      </form>

  <br>
  <div class="result"><p>
    <!-- ここにPHPの処理を書いて変換結果を出力してみよう -->
    <?php 
      // アルファベットの配列を作成する                                  $alphabets = [a,b,c,d,e,f,g,・・・・・ｚ];
      // 入力フォームの中身を取得する                                    $_POST['number']
      // 入力フォームのカンマ区切りの文字列を配列に格納し直す                "1,2,3,4,5"   →   $input_num = [1,2,3,4,5]
      // 上で作成した配列の要素分ループさせて数値をアルファベットに変換し出力   foreach と echo    

      // echo $_POST['number'];
      // "a"  →   $alphabets[0]
      // "b"  →   $alphabets[1]
      // "f"  →   $alphabets[5]

      // アルファベットの配列を定義  $alphabets = [a,b,c,d,e,f,g,・・・・・ｚ];
      // alphabets[0] → a
      // alphabets[1] → b
      $alphabets = range('a', 'z');
      
      if ( !empty($_POST['number']) ){
          // 入力フォームのカンマ区切りを配列に格納し直す  "1,2,3,4,5" → $input_num = [1,2,3,4,5]
          $input_num = explode(",", $_POST['number']);
          // print_r($input_num);

          // 入力フォームの数値を配列の要素分ループさせる
          foreach($input_num as $num) {
            // アルファベットの配列と入力フォームの配列の要素の数字を対応させて出力させる
            if (is_null($alphabets[$num-1])) {
              echo "( ꒪⌓꒪)ヘンカンデキナカッタ";
            } else {
              echo $alphabets[$num-1];
            }
          }
      }

    ?>
  </p></div>

  <br><br>
  <!-- 
  ▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼　
  【問題】文字数を数える 
  ▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲
  -->
  <div class="font01">【問題】文字数を数える</div>
    <!-- ここにHTMLでフォームを追加 -->
    <form action="practice.php" method="post">
      <input type="text" name="moji" class="text01" placeholder="文字を入力して文字数を数える">
      <input type="submit" class="btn-gradient-3d" value="文字数は？">
    </form>
  <br>
  <div class="result"><p>
    <!-- ここにPHPの処理を書いて変換結果を出力してみよう -->

    <?php
      if ( !empty($_POST['moji']) ){
        $result = str_replace(array(" ", "　"), "", $_POST['moji']);
        echo "入力した文字：". $_POST['moji']."★文字数：".mb_strlen($result);
      }
    ?>
  </p></div>

</body>
</html>
