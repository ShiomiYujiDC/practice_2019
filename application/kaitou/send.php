<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="stylesheet.css">
</head>
<body>
  <div class="main">
      <div class="form-item">■ 名前</div>
      <!-- nameを受け取りechoしましょう -->
      <?php
        echo $_POST['name'];
      ?>
      
      <div class="form-item">■ 内容</div>
      <!-- bodyを受け取りechoしましょう -->
      <?php
        echo $_POST['body'];
      ?>
      <div class="form-item">■ 果物を選択</div>
      <!-- fruits を受け取りechoしましょう -->
      <?php
        echo $_POST['fruits'];
      ?>
      <!-- upload を受け取りechoしましょう -->
      <div class="form-item">■ アプロード画像</div>
      <?php
        echo $_POST['upload'];
      ?>
  </div>
</body>
</html>