<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="stylesheet.css">
  </head>
<body>
  <div class="main">
    <!-- この下にformタグを書いていきましょう -->
    <form action="send.php" method="post">
      <section>
        <div class="form-item">名前を入力</div>
        <!-- ここにテキストボックスを追加 -->
        <input type="text" name="name"/>
      </section>
      <section>
        <div class="form-item">内容を入力</div>
        <!-- ここにテキストエリアを追加 -->
        <textarea name="body"></textarea>
      </section>
        <!-- ここにセレクトボックスを追加 -->
      <section>
        <div class="form-item">果物</div>
        <select name="fruits">
            <option value="未選択">未選択</option>
            <option value="りんご">りんご</option>
            <option value="みかん">みかん</option>
            <option value="バナナ">バナナ</option>
        </select>
      </section>
      <section>
      <div class="form-item">画像を選択</div>
        <input type="file" name="upload" value="画像をアップロード">
      </section>
        <input type="submit" value="送信">
    </form>
  </div>
</body>
</html>