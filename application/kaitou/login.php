<?php
    // 問１０．ログインフォームの値を取得しよう
    echo "ユーザID:" . $_POST['id'];
    echo "</br>";

    echo "パスワード:" . $_POST['password'];
    echo "</br>";

    // 問１１．フォームに値が入力されていない場合「フォームに値を入力してください」と表示しよう
    if(empty($_POST['id']) || empty($_POST['password'])) {
        echo "ユーザIDとパスワードを入力してください。";
        echo "</br>";
    } else {
        // 問１２．ファイルからパスワードを取得しよう
        $jsonContents = file_get_contents('./password.json');
        $json = json_decode($jsonContents);   

        // user に入ってる２つの配列のうち 0 番目のオブジェクトの id プロパティを取得したいとき
        echo "jsonファイルのユーザID：" . $json->user[0]->id;    
        echo "</br>";

        // user に入ってる２つの配列のうち 0 番目のオブジェクトの password プロパティを取得したいとき
        echo "jsonファイルのパスワード：" . $json->user[0]->password;   
        echo "</br>";  

        // 問１３．フォームと json のパスワードを比較して一致したらログインできるようにしよう
        for ($i=0; $i < count($json->user); $i++) {
            if ($_POST['id'] == $json->user[$i]->id) {
                if ($_POST['password'] == $json->user[$i]->password) {
                    header('Location: ./index.php');
                    exit();    
                }
            }
        } 
        // 問１３．別解

        // for ($i=0; $i < count($json->user); $i++) {
        //     if ($_POST['id'] == $json->user[$i]->id && $_POST['password'] == $json->user[$i]->password) {
        //         header('Location: ./index.php');
        //         exit();
        //     }
        // } 
        echo "ユーザIDまたは、パスワードが間違っています。もう一度入力し直してください。";
    }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href=https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css>
</head>
<body>
  <div class="main">
    <div class="container">
    <form action="login.php" method="POST">
        <div class="form-group">
            <label>ユーザID</label>
            <input type="text" class="form-control" name="id" placeholder="例：user1" required>
        </div>
        <div class="form-group">
            <label>パスワード</label>
            <input type="password" class="form-control" name="password">
        </div>
        <input type="submit" value="ログイン" class="btn btn-primary"/>
    </form>
    </div>
  </div>
</body>
</html>